class UsersController < ApplicationController
  def index
    @users = User.all
    render json: @users
  end

  def create
    @user = User.create(name: params[:name],email: params[:email])
    render json: @user
  end

  def update
    set_user
    @user.update_attributes(name: params[:name])
    render json: @user
  end

  def destroy
   set_user
    if @user.destroy
      head :no_content, status: :ok
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end
private
def set_user
@user = User.find(params[:id])
end
end

